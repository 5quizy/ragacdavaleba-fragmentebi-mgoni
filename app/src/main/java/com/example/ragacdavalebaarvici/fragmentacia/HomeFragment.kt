package com.example.ragacdavalebaarvici.fragmentacia

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.ragacdavalebaarvici.R


class HomeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var editTextAmount : EditText
    private lateinit var sendButton: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextAmount = view.findViewById(R.id.editTextAmount)
        sendButton = view.findViewById(R.id.sendButton)

        val controller = Navigation.findNavController(view)

        sendButton.setOnClickListener(){

            val ammountText= editTextAmount.text.toString()
            if (ammountText.isEmpty())
            return@setOnClickListener

            val ammount = ammountText.toInt()

            val action = HomeFragmentDirections.actionHomeFragmentToDashboardFragment(ammount)

            controller.navigate(action)


        }

    }


}