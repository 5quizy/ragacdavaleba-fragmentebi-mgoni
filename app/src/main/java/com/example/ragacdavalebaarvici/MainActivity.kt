package com.example.ragacdavalebaarvici

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.controls.Control
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationMenuView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView = findViewById<BottomNavigationMenuView>(R.id.bottomNavigationView)

        val Controller = findNavController(R.id.nav_host_fragment)

        val appBarConfig = AppBarConfiguration (
            setOf(
                R.id.homeFragment
            )


        )

        setupActionBarWithNavController(Controller, appBarConfig)
    }
}